defmodule MetrobusCdmx do
  import SweetXml

  @xml_path "./data/estaciones/doc.kml"
  @space_str ~r/\s+/

  def metro_lines() do
    xml = File.read!(@xml_path)
    stations = get_stations(xml)

    xml
    |> SweetXml.xpath(~x"//Folder/Placemark/description/text()"ls)
    |> Enum.map(&String.replace(&1, @space_str, " "))
    |> Enum.map(&html_to_map/1)
    |> Enum.map(fn line_info ->
      %Line{
        name: "Linea #{line_info["linea"]}",
        stations: Map.get(stations, String.to_integer(line_info["linea"]))
      }
    end)
    |> Enum.uniq()
  end

  defp get_stations(xml) do
    %{stations: stations} =
      SweetXml.xmap(xml,
        stations: [
          ~x"//Folder/Placemark"l,
          coordinates: ~x"./Point/coordinates/text()"s,
          description: ~x"./description/text()"s
        ]
      )

    stations
    |> Enum.reduce(%{}, fn station, acc ->
      station_data = String.replace(station.description, @space_str, " ") |> html_to_map()
      station_coords = String.replace(station.coordinates, @space_str, " ")

      Map.put(
        acc,
        String.to_integer(station_data["linea"]),
        [
          %Station{name: station_data["nombre"], coords: station_coords}
          | Map.get(acc, String.to_integer(station_data["linea"]), [])
        ]
      )
    end)
  end

  defp html_to_map(raw_data) do
    raw_data
    |> Floki.parse_document!()
    |> Floki.find("body table table tr")
    |> Enum.reduce(%{}, fn {_, _, [{_, _, [key]}, {_, _, value}]}, acc ->
      Map.put(
        acc,
        String.downcase(key),
        if(value == [], do: "", else: hd(value))
      )
    end)
  end
end

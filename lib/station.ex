defmodule Station do
  @moduledoc """
  Struct of the station
  """
  defstruct [:name, :coords]
end

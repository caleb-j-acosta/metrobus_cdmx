defmodule Line do
  @moduledoc """
  Struct of the line
  """
  defstruct [:name, :stations]
end
